package com.zyplayer.doc.manage.web.generator;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 暮光：城中城
 * @since 2018-12-05
 */
@RestController
@RequestMapping("/user-info")
public class GeneratorUserInfoController {

}
